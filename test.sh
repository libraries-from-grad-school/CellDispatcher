#!/bin/bash

RET=0
COUNT=0

while [ ${RET} -eq 0 ]
do
	./main 2>> results.csv
	RET=$?
	COUNT=$((COUNT+1))
	echo succeeded ${COUNT} times.
done

echo return value: ${RET}
