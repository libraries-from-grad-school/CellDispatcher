#ifndef NETWORKSERVER_H_
#define NETWORKSERVER_H_

class NetworkSession;

class NetworkServer
{
public:
	NetworkServer(int port);
	virtual ~NetworkServer();

// Listen methods, the non-blocking version creates a handler thread
	NetworkSession* listen_blocking();
	bool listen_noblock();

// Cancel a non-blocking listen
	void listen_stop();

// Get status bits
	bool isError();

private:
	int sock;
	
	bool error;
};

#endif /*NETWORKSERVER_H_*/
