/*****************************************************************************
 * (C) Oregon State University 2007
 *
 * ppu_lib.c - Process queue and dispatcher for SPU job managment
 * Author: William Dillon
 *****************************************************************************/

#include <stdio.h>
#include <errno.h>
#include <sched.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <libspe2.h>
#include <pthread.h>
#include <semaphore.h>

#include "cell_lib.h"

//static const char *red		= "\033[0;40;31m";
//static const char *green		= "\033[0;40;32m";
//static const char *yellow		= "\033[0;40;33m";
//static const char *blue		= "\033[0;40;34m";
//static const char *magenta	= "\033[0;40;35m";
//static const char *cyan		= "\033[0;40;36m";
//static const char *normal		= "\033[0m";

/*****************************************************************************
 * PPU thread argument structure
 *****************************************************************************/
typedef struct ppu_thread_args {
	spu_manager_t *manager;
	unsigned int   id;
} ppu_thread_args_t;	

/*****************************************************************************
 * Function to create a pthread function on the PPU (start the SPE thread)
 *****************************************************************************/
void *
ppu_pthread_function(void *arg) {
	ppu_thread_args_t	*args = (ppu_thread_args_t *)arg;
	spu_manager_t		*manager = args->manager;
	spe_context_ptr_t	 context;
	spu_task_data_t 	*task;
	spe_stop_info_t		 stop_info;
	unsigned int 		 entry;
	int					 fd = manager->pipe[args->id*2+1];
	
// Create SPE context
	if( (context = spe_context_create(0, NULL)) == NULL) {
		perror( "Failed creating context" );
		exit(1);
	}
	
// PPU dispatcher main loop
	while( (task = (spu_task_data_t *)get_top(&manager->queue)) != NULL ) {
					
	// Make sure that the task job is valid
		if( task->job == NULL ) {
			fprintf(stderr, "Got NULL SPU job, continuing.\n");
			free( task );
			continue;
		}

/*** BEGIN TIMING OF SPU TASK ***/
	// Get wall-clock time for job schedule
		clock_gettime(CLOCK_REALTIME, &task->scheduled);
		
	// Load the task function into the SPU
		if( spe_program_load( context, task->job) ) {
			perror( "Failed loading spu function dispatcher" );
			exit(1);
		}

	// Reset the spe_entry value (over-written by spe_context_run)
		entry = SPE_DEFAULT_ENTRY;	

//		fprintf(stderr, "SPE context create with arg: 0x%x.\n", task->arg);
		
	// Run SPU context with passed-in argument (to completion)
		if( spe_context_run(context, &entry, 0, task->arg, task->arg, &stop_info) < 0) {
			perror("Running of context failed");
			exit(1);
		}
		
	// Get wall-clock time for job completion
		clock_gettime(CLOCK_REALTIME, &task->completed);
/*** END TIMING OF SPU TASK ***/

		task->running_nsec  = 1000000000.0 * ((double)task->completed.tv_sec  - (double)task->scheduled.tv_sec) +
											 ((double)task->completed.tv_nsec - (double)task->scheduled.tv_nsec);
		task->inflight_nsec = 1000000000.0 * ((double)task->completed.tv_sec  - (double)task->enqueued.tv_sec)  +
											 ((double)task->completed.tv_nsec - (double)task->enqueued.tv_nsec);
		
//		fprintf(stderr, "Running time: %.0f nSec, inflight: %.0f nSec,\n", task->running_nsec, task->inflight_nsec);
		
// Async. send notification to the user logic of callback completion
		write(fd, &task, sizeof(void *));
	}

// Destroy SPU thread context
	if( spe_context_destroy(context) != 0 ) {
		perror( "Unable to destroy SPU context" );
	}
	
// If we are here we got a NULL SPU task which is a graceful shutdown
	int nullPtr = (int)NULL;
	write(fd, &nullPtr, sizeof(void *));	// Send termination notice to the callback thread
	pthread_exit(NULL);						// Terminate thread
	
	return NULL;
}

void *
callback_pthread_function(void *arg) {
	ppu_thread_args_t	*args = (ppu_thread_args_t *)arg;
	spu_manager_t		*manager = args->manager;
	spu_task_data_t 	*task;
	int					 fd = manager->pipe[args->id*2];
	
	while( true ) {
		// Read from callback request pipe
		if( read(fd, &task, sizeof(void *)) != sizeof(void *) ) {
			fprintf(stderr, "Incomplete or errored read from pipe: %s\n", strerror(errno));
			continue;
		}
		
		if( task == NULL ) {
			// If we are here we got a NULL SPU task which is a graceful shutdown
			pthread_exit(NULL);	// Terminate thread
		}
		
		// Pass notification of job completion to PPU logic
		if( task->callback != NULL ) {
			task->callback( manager, task );
		}		
		
		// Task has finished, free it's memory
		free( task );
	}
}

/*****************************************************************************
 * Start the function dispatcher on the n requested SPUs,
 * Initialize the Priority Queue, and
 * Start the PPU-side function dispatcher
 *****************************************************************************/
int
start_spus(spu_manager_t *manager, int number) {
	int i;
	

	if( pthread_mutex_init(&manager->mutex, NULL) ) {
		perror("Initializing thread mutex");
		return -1;
	}
	
/*** CRITICAL SECTION BEGIN ***/
	if( pthread_mutex_lock(&manager->mutex) ) {
		perror("Locking thread mutex");
		return -1;
	}

// Query the OS for the number of phyiscal SPE's			
	manager->num_spu_threads = spe_cpu_info_get(SPE_COUNT_USABLE_SPES, -1);
																								  
// Determine the number of threads to create (if number == 0 already equal to manager->spu_threads).
	if( number > 0 ) {
		manager->num_spu_threads = (number < manager->num_spu_threads)? number : manager->num_spu_threads;
		if( number != manager->num_spu_threads ) {
			fprintf(stderr, "Truncating requested threads to physical threads: %d\n", manager->num_spu_threads);
		}
	}

	printf("Starting SPU manager on %d SPU's.\n", manager->num_spu_threads);
	
// Initialize the priority queue
	if( init_queue( &manager->queue ) != 0 ) {
		fprintf(stderr, "Unable to initialize the priority queue, bailing.\n");
		return -1;
	}

// Allocate file descriptors for pipes
	manager->pipe = (int *)malloc(sizeof(int) * manager->num_spu_threads * 2);
	if( manager->pipe == NULL ) {
		perror("Allocating array for pipe file descriptors");
		return -1;
	}
	
// Allocate thread argument structures
	ppu_thread_args_t *args = (ppu_thread_args_t *)malloc(sizeof(ppu_thread_args_t) * manager->num_spu_threads);
	if( args == NULL ) {
		perror("Allocating array thread arguments");
		return -1;
	}

// Start as many PPU threads for dispatch and callback as we want schedulable SPU threads
	manager->SPU_threads = (pthread_t *)malloc(sizeof(pthread_t) * manager->num_spu_threads);	
	manager->callBack_threads = (pthread_t *)malloc(sizeof(pthread_t) * manager->num_spu_threads);	

	for( i = 0; i < manager->num_spu_threads; i++ ) {
		args[i].manager = manager;
		args[i].id = i;
		
		if( pipe( &manager->pipe[i*2] ) == -1 ) {
			fprintf(stderr, "Error creating pipe for thread %d: %s\n", i, strerror(errno));
			return -1;
		}		
	}
	
	for( i = 0; i < manager->num_spu_threads; i++ ) {		
		if( pthread_create(&manager->SPU_threads[i], NULL, &ppu_pthread_function, &args[i]) ) {
			perror( "Failed creating thread" );
			return -1;
		}		

		if( pthread_create(&manager->callBack_threads[i], NULL, &callback_pthread_function, &args[i]) ) {
			perror( "Failed creating thread" );
			return -1;
		}
	}
	
	
					
					
// Unlock the mutex 
	if( pthread_mutex_unlock(&manager->mutex) ) {
		perror("Unlocking thread mutex");
		return -1;
	}
/*** CRITICAL SECTION END ***/

	return(manager->num_spu_threads);
}

/*****************************************************************************
 * Adds a job to the priority queue
 *****************************************************************************/
int
addjob_spu(	spu_manager_t *manager, int priority,
			spe_program_handle_t *job, void *arg,
			void(*callback)(spu_manager_t *, spu_task_data_t *),
			void *udata ) {
	
	spu_task_data_t *tag = (spu_task_data_t *)malloc(sizeof(spu_task_data_t));
	if( tag == NULL ) {
		perror("Allocating RAM for SPU process entry");
		return 1;
	}
	
	tag->job		= job;
	tag->arg		= arg;	
	tag->priority	= priority;
	tag->callback	= callback;
	tag->tag		= udata;
	
// Get wall-clock time for job enquement
	clock_gettime(CLOCK_REALTIME, &tag->enqueued);
	enqueue_block(&manager->queue, priority, tag);
	
	return 0;
}

/*****************************************************************************
 * Halts SPU execution and cleans up all state (terminates threads, etc)
 *****************************************************************************/
int
cleanup_spus(spu_manager_t *manager, int priority) {
	int i;
	
	void *value;
	
	// Send a terminate message to the dispatcher thread
	for( i = 0; i < manager->num_spu_threads * 10; i++ ) {
		enqueue_block(&manager->queue, priority, NULL);
	}
			
	// Wait for the SPU_thread to complete execution
	for( i = 0; i < manager->num_spu_threads; i++ ) {
	
	// Join PPU thread presumably the one that got the message
		if( pthread_join(manager->SPU_threads[i], &value) ) {
			perror( "Failed to join thread" );
			exit(1);
		}

		if( pthread_join(manager->callBack_threads[i], &value) ) {
			perror( "Failed to join thread" );
			exit(1);
		}
	} 

	if( pthread_mutex_destroy(&manager->mutex) ) {
		perror("Destroying thread mutex");
	}

	// Free the thread array
	free( manager->SPU_threads );
	return(0);
}
