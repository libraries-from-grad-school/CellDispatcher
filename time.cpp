/*
 *	time.cpp
 *
 *	Created by William Dillon on 3/19/08.
 *	Copyright 2008 Oregon State University. All rights reserved.
 *
 */

#include <assert.h>
#include <CoreServices/CoreServices.h>
#include <mach/mach.h>
#include <mach/mach_time.h>
#include "time.h"

long clock_gettime(clockid_t which_clock, struct timespec *tp)
{
	uint64_t		absTime;
	Nanoseconds		elapsedNano;
	
	if( which_clock != CLOCK_REALTIME ) {
		return -EINVAL;
	}
	
	if( tp == NULL ) {
		return -EFAULT;
	}
	
	absTime = mach_absolute_time();
		
	// Convert to nanoseconds.
	
	// Have to do some pointer fun because AbsoluteToNanoseconds
	// works in terms of UnsignedWide, which is a structure rather
	// than a proper 64-bit integer.
	
	elapsedNano = AbsoluteToNanoseconds( *(AbsoluteTime *) &absTime );	
	
	absTime = *((uint64_t *)&elapsedNano);
	
// Find the number of seconds through integer division of nanoseconds and 1 billion
	tp->tv_sec  = absTime / 1000000000;
// And isolate the nanoseconds by taking the remainder
	tp->tv_nsec = absTime % 1000000000;
	
	return 0;
}