#-----------------------------------
# Oregon State University 2007
# Author: William Dillon
#-----------------------------------

#####################################################
# Subdirectories
#####################################################

DIRS		:= spu

#####################################################
# Target
#####################################################

PROGRAM_ppu	:= main

#####################################################
# Local Defines
#####################################################

IMPORTS		:= spu/spu_lib.a -lspe2 -lpthread

PPU_TARGETS	:= true

CPPFLAGS_gcc := -g -Os
LDFLAGS +=-Wl

#####################################################
# make.footer - For Cell SDK
#####################################################

ifdef CELL_TOP
	include $(CELL_TOP)/buildutils/make.footer
else
	include /opt/cell/sdk/buildutils/make.footer
endif
