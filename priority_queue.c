/*****************************************************************************
 * (C) Oregon State University 2007
 *
 * priority_queue.c - Priority Queue for use with the SPU dispatcher function
 * Author: William Dillon
 *****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>

#include "priority_queue.h"

#ifdef TRACE
static int count;
static trace_t trace[TRACE];
#endif

/*****************************************************************************
 ************************** PRIVATE FUNCTIONS ********************************
 *****************************************************************************/

/*****************************************************************************
 * add_queue - Once a entry is created add it to the heap structure
 *****************************************************************************/
static void
add_queue(priority_queue_data_t *queue, priority_queue_entry_t *entry)
{
	int child  = queue->num_elements;
	int parent = (child - 1)/2;

	// Make space for the new element by using the "sift-up" operation	
	while(	child > 0 && queue->heap[parent].priority < entry->priority ) {	

		// Heap property is not held, shuffle the parent down
		queue->heap[child].priority = queue->heap[parent].priority;
		queue->heap[child].tag	    = queue->heap[parent].tag;

		// Prepare for the subsequent test (and possible swap)
		child = parent;
		parent = (child - 1)/2;
		
#ifdef TRACE
		trace[count].swaps++;
#endif
	}
	
	// Write the element into the queue in it's place
	queue->heap[child].priority = entry->priority;
	queue->heap[child].tag	  	= entry->tag;
	
	// Insert is complete, represent the larger heap size
	queue->num_elements += 1;
	
	return;
}

/*****************************************************************************
 * del_top - Move the last element in the queue to the root and "sift down"
 * Including support function, largest child
 *****************************************************************************/
static int largest_child(priority_queue_data_t *queue, int parent);

static void
del_top(priority_queue_data_t *queue)
{
	int parent = 0;
	int child;
	int kvalue = queue->heap[queue->num_elements-1].priority;

	// Begin at the root
	child = largest_child(queue, parent);
	
	// Traverse the tree from the root through each child that is the largest
	while(	child >= 0 && kvalue < queue->heap[child].priority ) {

		// Shuffle the parent down
		queue->heap[parent].priority = queue->heap[child].priority;
		queue->heap[parent].tag		 = queue->heap[child].tag;

		// Get ready for the next iteration
		parent = child;
		child  = largest_child(queue, parent);

#ifdef TRACE
		trace[count].swaps++;
#endif
	}
	
	// Put the new element into the found location
	queue->heap[parent] = queue->heap[queue->num_elements - 1];
	
// Delete top complete, reflect in num_elements.
	queue->num_elements -= 1;
	return;
}

static int
largest_child(priority_queue_data_t *queue, int parent)
{
	int index = queue->num_elements - 1;
	int child = 2 * parent + 1;
	
	if( child + 1 <= index && queue->heap[child].priority < queue->heap[child + 1].priority ) {
		child = child + 1;
	}
	
	// Make sure we haven't gone out of bounds
	if( child > index ) {
		return -1;
	} else {
		return child;
	}
}


/*****************************************************************************
 *************************** PUBLIC FUNCTIONS ********************************
 *****************************************************************************/

/*****************************************************************************
 * init_queue - Initialize queue data structures
 *****************************************************************************/
int
init_queue( priority_queue_data_t *queue ) {
	int i;
	
	// Initialize and lock the pthread mutex
	if( pthread_mutex_init(&queue->mutex, NULL) != 0 ) {
		perror("Unable to initialize mutex");
		return 1;
	}
	
	if( pthread_mutex_lock(&queue->mutex) != 0 ) {
		perror("Unable to lock mutex");
		return 1;
	}	

	if( pthread_cond_init(&queue->cond, NULL) != 0 ) {
		perror("Unable to initialize condition variable");
		return 1;
	}
	
#ifdef TRACE
	count = 0;
#endif
	
	// Set known values for the sentry.
	queue->sentry.tag	   = (void*)0xA5A5A5A5;
	queue->sentry.priority = 42;
	
	// Initialize the number of elements to 0
	queue->num_elements = 0;
	
	// Initialize each member of the queue
	for( i = 0; i < SIZE; i++ ) {
		// Fill the index array with indicies to the struct_array
		queue->heap[i].priority = 0;
		queue->heap[i].tag = NULL;
	}

	if( pthread_mutex_unlock(&queue->mutex) != 0 ) {
		perror("Unable to unlock mutex");
		return 1;
	}
	
	return 0;
}
 
/*****************************************************************************
 * enqueue_block - Add an item to the queue blocking on full queue
 *****************************************************************************/
void
enqueue_block(priority_queue_data_t *queue, int priority, void *tag ) {
	priority_queue_entry_t entry;

/*** Critical Section Begin ***/
	pthread_mutex_lock(&queue->mutex);

	// Use a condition variable to ensure a non-full queue
	while( queue->num_elements >= SIZE ) {
#ifdef TRACE
		fprintf(stderr, "Waiting on condition variable to enqueue.\n");
#endif		
		pthread_cond_wait( &queue->cond, &queue->mutex );
	}

	// Set the entry's user fields
	entry.priority = priority;
	entry.tag = tag;
	
#ifdef TRACE
	trace[count].op = ENQUEUE;
	trace[count].fill_level = queue->num_elements;
	trace[count].priority = priority;
	trace[count].tag = tag;
	count++;
#endif	

	// Check the sentry value
	if( queue->sentry.tag	   != (void*)0xA5A5A5A5 ||
	    queue->sentry.priority != 42 ) {
		fprintf(stderr, "Sentry value changed!: ");
		exit(EXIT_FAILURE);
	}	

	// Add an entry to the hash table with user fields
	add_queue(queue, &entry);
	
	pthread_mutex_unlock(&queue->mutex);
/*** Critical Section End   ***/

	// Signal the empty condition variable to a change in state
	pthread_cond_broadcast( &queue->cond );

#ifdef TRACE
	fprintf(stderr, "Traced Enqueue operation, fill level: %d, priority: %d (%d swaps), tag: 0x%x, operation: %d.\n",
					trace[count-1].fill_level, trace[count-1].priority, trace[count-1].swaps, (int)trace[count-1].tag, count );
#endif

}

/*****************************************************************************
 * enqueue_nonBlock - Add an item to the queue blocking on full queue
 *****************************************************************************/
bool
enqueue_nonBlock(priority_queue_data_t *queue, int priority, void *tag ) {
	priority_queue_entry_t entry;

/*** Critical Section Begin ***/
	pthread_mutex_lock(&queue->mutex);

	// Conditions not met, return failure
	if( queue->num_elements >= SIZE ) {
		pthread_mutex_unlock(&queue->mutex);		
		/*** Critical Section End   ***/
		return FALSE;

	// Conditions met, continue with operation
	} else {
		
		// Set the entries user fields
		entry.priority = priority;
		entry.tag = tag;
		
#ifdef TRACE
		trace[count].op = ENQUEUE;
		trace[count].fill_level = queue->num_elements;
		trace[count].priority = priority;
		trace[count].tag = tag;
		count++;
#endif		

		// Check the sentry value
		if( queue->sentry.tag	   != (void*)0xA5A5A5A5 ||
		    queue->sentry.priority != 42 ) {
			fprintf(stderr, "Sentry value changed!: ");
			exit(EXIT_FAILURE);
		}
		
		// Add queue entries to heap
		add_queue(queue, &entry);
	
		pthread_mutex_unlock(&queue->mutex);
/*** Critical Section End   ***/

		// Signal the empty condition variable to a change in state
		pthread_cond_broadcast( &queue->cond );

#ifdef TRACE
	fprintf(stderr, "Traced Enqueue operation, fill level: %d, priority: %d, tag: 0x%x, operation: %d.\n",
					trace[count-1].fill_level, trace[count-1].priority, (int)trace[count-1].tag, count );
#endif

		return TRUE;
	}
}

/*****************************************************************************
 * get_top - Retrieve the highest priority item from the queue (and delete)
 * 
 * RETURN VALUE: the tag of the top item, NULL if queue is empty
 *****************************************************************************/
void *
get_top(priority_queue_data_t *queue) {	
	void *tag;
		
/*** Critical Section Begin ***/
	pthread_mutex_lock(&queue->mutex);

	// ensure a non-empty queue
	while( queue->num_elements <= 0 ) {
#ifdef TRACE
		fprintf(stderr, "Waiting on condition variable to dequeue (%ld, %d, 0x%x).\n", queue->num_elements, count, queue);
#endif
		pthread_cond_wait( &queue->cond, &queue->mutex );
	}

#ifdef TRACE
	trace[count].op = DEQUEUE;
	trace[count].fill_level = queue->num_elements;
	trace[count].priority = queue->heap[0].priority;
	trace[count].tag = queue->heap[0].tag;
	count++;
#endif
	
	// Check the sentry value
	if( queue->sentry.tag	   != (void*)0xA5A5A5A5 ||
	    queue->sentry.priority != 42 ) {
		fprintf(stderr, "Sentry value changed!: ");
		exit(EXIT_FAILURE);
	}	
	
	// Store the user value of the current root
	tag = queue->heap[0].tag;

	// Remove the root from the queue
	del_top(queue);

	pthread_mutex_unlock(&queue->mutex);
/*** Critical Section End   ***/

	// Signal the condition variable to a change in state
	pthread_cond_broadcast( &queue->cond );

#ifdef TRACE
	fprintf(stderr, "Traced Dequeue operation, fill level: %d, priority: %d, tag: 0x%x, operation: %d.\n",
					trace[count-1].fill_level, trace[count-1].priority, (int)trace[count-1].tag, count );
#endif
					

	return tag;
}

/*****************************************************************************
 ************************** TEST MAIN FUNCTION *******************************
 *****************************************************************************/

#define QUEUE_TEST
#ifdef  QUEUE_TEST
//#define THREADED_TEST
#ifdef  THREADED_TEST

static pthread_mutex_t producer_mutex = PTHREAD_MUTEX_INITIALIZER;
static pthread_mutex_t consumer_mutex = PTHREAD_MUTEX_INITIALIZER;

void *
producer_thread( void *void_pointer ) {
	int i, number;
	priority_queue_data_t *queue = (priority_queue_data_t *)void_pointer;

	pthread_mutex_lock(&producer_mutex);
	fprintf(stderr, "Producer past mutex.\n");
	
	srand(1);
	
	for( i = 0; i < SIZE; i++ ) {
		number = rand();
		enqueue_block(queue, number, (void *)number);
	}
	
	return NULL;
}

void *
consumer_thread( void *void_pointer ) {
	int i, number, results[SIZE];

	priority_queue_data_t *queue = (priority_queue_data_t *)void_pointer;

	pthread_mutex_lock(&consumer_mutex);	
	
	fprintf(stderr, "Consumer past mutex.\n");
	// We are the consumer
	for( i = 0; i < SIZE; i++ ) {
		results[i] = (int)get_top(queue);
	}

//	for( i = 0; i < SIZE; i++ ) {
//		printf("Results[%d]: %d.\n", i, results[i]);
//	}
	
	return NULL;
}
#endif

int
main() {
	int i;

	priority_queue_data_t queue;
	init_queue(&queue);
	
	sleep(1);
	
#ifdef THREADED_TEST
	pthread_t producer, consumer;

	// Try to make sure that the producer and consumer don't start
	pthread_mutex_lock(&producer_mutex);
	pthread_mutex_lock(&consumer_mutex);

	// Start the producer and consumer threads
	pthread_create(&consumer, NULL, consumer_thread, (void *)&queue);
	pthread_create(&producer, NULL, producer_thread, (void *)&queue);
	fprintf(stderr, "Started producer and consumer threads.\n");

	// Allow the producer and consumer to begin working
	pthread_mutex_unlock(&producer_mutex);
	pthread_mutex_unlock(&consumer_mutex);

	fprintf(stderr, "Allowed producer and consumer to begin working.\n");

	// Join the threads once they terminate
	void *result;
	if( pthread_join(producer, &result) != 0 ) {
		perror("Joining producer thread");
		exit(EXIT_FAILURE);
	}
	if( pthread_join(consumer, &result) != 0 ) {
		perror("Joining consumer thread");
		exit(EXIT_FAILURE);
	}
	
	fprintf(stderr, "Producer and consumer finished working.\n");
	
#ifdef TRACE
	fprintf(stderr, "Trace of operations:\n");
	for( i = 0; i < SIZE*2; i++ ) {
		fprintf(stderr, "Operation %d: ", i);
		if( trace[i].op == ENQUEUE ) {
			fprintf(stderr, "Enqueue with %d items in the heap, object priority: %d, tag: 0x%x, took %d swaps to maintain heap order.\n",
					trace[i].fill_level, trace[i].priority, trace[i].tag, trace[i].swaps);
		} else {
			fprintf(stderr, "Dequeue with %d items in the heap, object priority: %d, tag: 0x%x.\n",
					trace[i].fill_level, trace[i].priority, trace[i].tag);
		}
	}
#endif
	
	fprintf(stderr, "For posterity, sentry tag: 0x%X, priority: %d.\n", queue.sentry.tag, queue.sentry.priority);
	
	exit(EXIT_SUCCESS);

#else
	// Test sequence from "Data Structures Using C", page 344
	enqueue_block(&queue, 25, (void*)0x25);
	enqueue_block(&queue, 57, (void*)0x57);
	enqueue_block(&queue, 48, (void*)0x48);
	enqueue_block(&queue, 37, (void*)0x37);
	enqueue_block(&queue, 12, (void*)0x12);
	enqueue_block(&queue, 92, (void*)0x92);
	enqueue_block(&queue, 86, (void*)0x86);
	enqueue_block(&queue, 33, (void*)0x33);
	
	for( i = 0; i < 8; i++ ) {
		printf("De-queueing: (0x%x).\n", get_top(&queue));
	}
#endif
	return 1;
}

#endif
