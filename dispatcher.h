#ifndef THREAD_MANAGER_H
#define THREAD_MANAGER_H

// Make sure that either heap or FIFO is defined (default FIFO)
#ifndef PRIORITY_FIFO
#ifndef PRIORITY_HEAP
#define PRIORITY_HEAP
#endif
#include "priority_heap.h"
typedef priority_heap_t queue_t;
#else
#include "priority_fifo.h"
typedef priority_fifo_t queue_t;
#endif

#ifndef DEFAULT_THREADS
#define DEFAULT_THREADS 10
#endif

struct thread_manager;
struct task_data;
typedef struct thread_manager thread_manager_t;
typedef struct task_data task_data_t;
typedef void(*thread_manager_function_t)(thread_manager_t *, task_data_t *);

/*****************************************************************************
 * Thread Manager structure
 *****************************************************************************/
struct thread_manager {

// Thread count and data
	int 		 num_threads;
	pthread_t	*threads;
	pthread_t	*callBack_threads;
	
	// Listing of pipe endpoints for ipc
	int	*pipe;
	
// Mutex to be sure we don't try to join threads before they finish
	pthread_mutex_t mutex;
	
// Job queue
#ifdef	PRIORITY_HEAP
	priority_heap_t	queue; // Heap queue for SPU jobs
#else//	PRIORITY_FIFO
	priority_fifo_t	queue; // FIFO queue for SPU jobs
#endif

};

/*****************************************************************************
 * SPU task data structure
 *****************************************************************************/
//struct task_data;
struct task_data {

// Runtime metrics
	struct timespec	enqueued; // Start time	
	struct timespec	scheduled; // Start time
	struct timespec	completed; // Completion time
	
	double inflight_nsec;
	double  running_nsec;
//	unsigned long long int inflight_nsec;	// Total wall-clock enqueued time
//	unsigned long long int running_nsec;	// Total wall-clock run time (including PPU)
	
	// Process priority
	int	priority;
	
	// SPU function name and arguments
	thread_manager_function_t job;

	void *arg;
		
	// Managment Call-backs (execute in their own PPU thread)
	thread_manager_function_t callback;

	void *tag;
//	void *retval;

};

/*****************************************************************************
 * PUBLIC INTERFACE FUNCTION PROTOTYPES
 *****************************************************************************/
int start_threads(	thread_manager_t *manager, int number);

int addjob(	thread_manager_t *manager,			 int  priority,
			thread_manager_function_t job,		void *arg,
			thread_manager_function_t callback, void *udata );

int cleanup(thread_manager_t *manager, int priority);

#endif //THREAD_MANAGER_H