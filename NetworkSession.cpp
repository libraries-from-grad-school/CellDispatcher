#include "NetworkSession.h"

#ifdef OSX

#else
//#include <error.h>
#endif

#include <time.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <errno.h>
#include <netdb.h>
#include <string.h>
#include <unistd.h>
#include <stdio.h>

int ignoreSigPipe()
{
	sigset_t newSigSet;
	
	sigemptyset( &newSigSet );
	sigaddset( &newSigSet, SIGPIPE );
	return sigprocmask( SIG_BLOCK, &newSigSet, NULL );
}

NetworkSession::NetworkSession(char *hostName, unsigned int portNum)
{
	int sock, retval;
	error = true;
	
	if( ignoreSigPipe() != 0 )
		fprintf(stderr, "Unable to ignore SIGPIPE\n");
	
	hp = gethostbyname( hostName );
	if( hp == NULL ) {
		perror( "Looking up host address" );
		return;
	}
	
	sock = socket( AF_INET, SOCK_STREAM, 0 );
	if( sock == -1 ) {
		perror( "Opening Socket" );
		return;
	}
	
	memcpy((char *)&server.sin_addr, hp->h_addr_list[0], hp->h_length);

	server.sin_port = htons((short)portNum);
	server.sin_family = AF_INET;
	
	do {
		retval = connect( sock, (struct sockaddr *)&server, sizeof(server));
	} while( retval == -1 && (errno == EINTR) );
	
	if( retval == -1 ) {
		close(sock);
		perror("Unable to connect");
	} else {
		fileDescriptor = sock;
		connected = true;
		error = false;
	}
	
	return;
}

NetworkSession::NetworkSession(int newFileDescriptor, struct sockaddr_in* net_client)
{
	error = true;

	if( ignoreSigPipe() != 0 )
		fprintf(stderr, "Unable to ignore SIGPIPE\n");

	hp = gethostbyaddr((char *)&(net_client->sin_addr.s_addr), 4, AF_INET);
	
	fileDescriptor = newFileDescriptor;
	
	if( hp == NULL ) {
		strcpy( hostName, "Unknown" );
	} else {
		strcpy( hostName, hp->h_name );
	}
	
	fprintf(stderr, "Opened network session with %s.\n", hostName );
	
	connected = true;
	error = false;

	return;
}

bool
NetworkSession::isError()
{
	return error;
}

bool
NetworkSession::isConnected()
{
	return connected;
}

char*
NetworkSession::getHostName()
{
	if( connected ) {
		return hostName;
	} else {
		return NULL;
	}
}

int
NetworkSession::getFileDescriptor()
{
	return fileDescriptor;
}

int
NetworkSession::writeBytes(void* buffer, int length)
{
	int retval = send( fileDescriptor, buffer, length, 0 );
	
	if( retval >= 0 )
		bytesWritten += retval;

	return retval;
}

int
NetworkSession::readBytes(void* buffer, int length)
{
	int retval;
	int	index = 0;

	struct timespec started, completed;
	
	char *buff = (char *)buffer;
	
// Ensure no 0-byte reads (this would force an erroneous error state)
	if( length == 0 ) {
		return 0;
	}

	fprintf(stderr, "Attempting read of %d bytes: ", length);
	fflush(stderr);
	
/*** BEGIN TIMING OF NETWORK TRANSFER ***/
	clock_gettime(CLOCK_REALTIME, &started);
	
	do {
		if( length - index == 0 ) {
			fprintf(stderr, "WTF, why a 0 byte read?\n");
			return index;
		}
		
		retval = read( fileDescriptor, &buff[index], length-index);

		if( retval > 0 ) {
			index += retval;
			bytesRead += retval;
		} else if( retval == 0 ) {
			fprintf(stderr, "Connection terminated, exiting.\n");
			errno = EPIPE;
			return 0;
		} else {
			if( errno != EINTR ) {
				error = true;
				perror( "Reading Bytes" );
				return 0;
			}
		}
	} while (index < length);

	/*** END TIMING OF NETWORK TRANSFER ***/
	clock_gettime(CLOCK_REALTIME, &completed);
	
	float running_nsec  = 1000000000.0 * ((double)completed.tv_sec  - (double)started.tv_sec) +
										 ((double)completed.tv_nsec - (double)started.tv_nsec);
	
	fprintf(stderr, "Completed in %f seconds (%1.2f Mb/sec).\n", running_nsec/1000000000.0, ((float)length)/(running_nsec/1000.0));
	
	return index;
}

unsigned int
NetworkSession::getBytesWritten()
{
	return bytesWritten;
}

unsigned int
NetworkSession::getBytesRead()
{
	return bytesRead;
}

NetworkSession::~NetworkSession()
{
	close( fileDescriptor );
	
	return;
}
