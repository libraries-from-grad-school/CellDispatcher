#ifndef PPU_LIB_H_
#define PPU_LIB_H_

#include <libspe2.h>

#include "priority_queue.h"

/*****************************************************************************
 * SPU Manager structure
 *****************************************************************************/
typedef struct spu_manager {

// Thread count and data
	int 		 num_spu_threads;
	pthread_t	*SPU_threads;
	pthread_t	*callBack_threads;
	
	// Listing of pipe endpoints for ipc
	int	*pipe;
	
// Mutex to be sure we don't try to join threads before they finish
	pthread_mutex_t mutex;
	
// Job queue
	priority_queue_data_t queue; // Queue for SPU jobs

} spu_manager_t;

/*****************************************************************************
 * SPU task data structure
 *****************************************************************************/
struct spu_task_data;

typedef struct spu_task_data {
	// Runtime metrics
	struct timespec	enqueued; // Start time	
	struct timespec	scheduled; // Start time
	struct timespec	completed; // Completion time
	
	double inflight_nsec;
	double  running_nsec;
//	unsigned long long int inflight_nsec;	// Total wall-clock enqueued time
//	unsigned long long int running_nsec; // Total wall-clock run time (including PPU)
	
	// Process priority
	int	priority;
	
	// SPU function name and arguments
	spe_program_handle_t *job;
	void *arg;
		
	// Managment Call-backs (execute in their own PPU thread)
	void(*callback)(spu_manager_t *manager, struct spu_task_data *task);
	void *tag;
//	void *retval;
	
} spu_task_data_t;

/*****************************************************************************
 * PUBLIC INTERFACE FUNCTION PROTOTYPES
 *****************************************************************************/
int start_spus(spu_manager_t *manager, int number);
int addjob_spu(	spu_manager_t *manager, int priority,
				spe_program_handle_t *job, void *arg,
				void(*callback)(spu_manager_t *, spu_task_data_t *),
				void *udata );
int cleanup_spus(spu_manager_t *manager, int priority);

#endif /*PPU_LIB_H_*/
