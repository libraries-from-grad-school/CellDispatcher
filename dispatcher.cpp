/*****************************************************************************
 * (C) Oregon State University 2007
 *
 * ppu_lib.c - Process queue and dispatcher for SPU job managment
 * Author: William Dillon
 *****************************************************************************/

#include <stdio.h>
#include <errno.h>
#include <sched.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <pthread.h>
#include <semaphore.h>
#include "time.h"
#include "dispatcher.h"

//static const char *red		= "\033[0;40;31m";
//static const char *green		= "\033[0;40;32m";
//static const char *yellow		= "\033[0;40;33m";
//static const char *blue		= "\033[0;40;34m";
//static const char *magenta	= "\033[0;40;35m";
//static const char *cyan		= "\033[0;40;36m";
//static const char *normal		= "\033[0m";

/*****************************************************************************
 * PPU thread argument structure
 *****************************************************************************/
typedef struct thread_args {
	thread_manager_t *manager;
	unsigned int   id;
} thread_args_t;	

inline double get_nSeconds(struct timespec *start, struct timespec *stop) {
	return 1000000000.0 * ((double)stop->tv_sec  - (double)start->tv_sec) +
						  ((double)stop->tv_nsec - (double)start->tv_nsec);
}

/*****************************************************************************
 * Function to create a pthread function on the PPU (start the SPE thread)
 *****************************************************************************/
void *
compute_thread_function(void *arg) {
	thread_args_t		*args = (thread_args_t *)arg;
	thread_manager_t	*manager = args->manager;
	task_data_t			*task;
	int					 fd = manager->pipe[args->id*2+1];
	double				 running = 0.;
	double				 waiting = 0.;
	double				 overhead = 0.;
	struct timespec		 started;
	struct timespec		 stopped;
	
	
	while( 1 ) {

	// Get job from queue (timed)
		clock_gettime(CLOCK_REALTIME, &started);
		if( (task = (task_data_t *)get_top(&manager->queue)) != NULL )
			break;
		clock_gettime(CLOCK_REALTIME, &stopped);
		waiting += get_nSeconds(&started, &stopped);
		
	// Make sure that the task job is valid
		if( task->job == NULL ) {
			fprintf(stderr, "Got NULL job, continuing.\n");
			free( task );
			continue;
		}

	// Run job (timed)
		clock_gettime(CLOCK_REALTIME, &task->scheduled);
		task->job(manager, task);
		clock_gettime(CLOCK_REALTIME, &task->completed);
		running += get_nSeconds(&task->scheduled, &task->completed);

		task->running_nsec  = get_nSeconds(&task->scheduled, &task->completed);
		task->inflight_nsec = get_nSeconds(&task->enqueued,  &task->completed);

	// Notify callback function of job completion
		clock_gettime(CLOCK_REALTIME, &task->scheduled);
		write(fd, &task, sizeof(void *));
		clock_gettime(CLOCK_REALTIME, &task->completed);
		overhead += get_nSeconds(&task->scheduled, &task->completed);
	}
		
// If we are here we got a NULL task, which is a graceful shutdown
	int nullPtr = (int)NULL;
	write(fd, &nullPtr, sizeof(void *));	// Send termination notice to the callback thread

	pthread_exit(NULL);						// Terminate thread
	
	return NULL;
}

void *
callback_thread_function(void *arg) {
	thread_args_t		*args = (thread_args_t *)arg;
	thread_manager_t	*manager = args->manager;
	task_data_t			*task;
	int					 fd = manager->pipe[args->id*2];
	
	while( 1 ) {
		// Read from callback request pipe
		if( read(fd, &task, sizeof(void *)) != sizeof(void *) ) {
			fprintf(stderr, "Incomplete or errored read from pipe: %s\n", strerror(errno));
			continue;
		}
		
		if( task == NULL ) {
			// If we are here we got a NULL task which is a graceful shutdown
			pthread_exit(NULL);	// Terminate thread
		}
		
		// Pass notification of job completion to user logic
		if( task->callback != NULL ) {
			task->callback( manager, task );
		}		
		
		// Task has finished, free it's memory
		free( task );
	}
}

/*****************************************************************************
 * Start the function dispatcher on the n requested threads,
 * Initialize the Priority Queue, and start the function dispatcher
 *****************************************************************************/
int
start_threads(thread_manager_t *manager, int number) {
	int i;

	if( pthread_mutex_init(&manager->mutex, NULL) ) {
		perror("Initializing thread mutex");
		return -1;
	}
	
/*** CRITICAL SECTION BEGIN ***/
	if( pthread_mutex_lock(&manager->mutex) ) {
		perror("Locking thread mutex");
		return -1;
	}

//TODO: Query the OS for the number of phyiscal cores
																								  
// Determine the number of threads to create (if number == 0 already equal to manager->spu_threads).
	if( number > 0 ) {
		manager->num_threads = number;
	} else {
		manager->num_threads = DEFAULT_THREADS;
	}

	printf("Starting thread manager on %d threads's.\n", manager->num_threads);
	
// Initialize the priority queue
	if( init_queue( &manager->queue ) != 0 ) {
		fprintf(stderr, "Unable to initialize the priority queue, bailing.\n");
		return -1;
	}

// Allocate file descriptors for pipes
	manager->pipe = (int *)malloc(sizeof(int) * manager->num_threads * 2);
	if( manager->pipe == NULL ) {
		perror("Allocating array for pipe file descriptors");
		return -1;
	}
	
// Allocate thread argument structures
	thread_args_t *args = (thread_args_t *)malloc(sizeof(thread_args_t) * manager->num_threads);
	if( args == NULL ) {
		perror("Allocating array thread arguments");
		return -1;
	}

// Start as many compute threads for dispatch and callback as we want for compute threads
	manager->threads = (pthread_t *)malloc(sizeof(pthread_t) * manager->num_threads);	
	manager->callBack_threads = (pthread_t *)malloc(sizeof(pthread_t) * manager->num_threads);	

	for( i = 0; i < manager->num_threads; i++ ) {
		args[i].manager = manager;
		args[i].id = i;
		
		if( pipe( &manager->pipe[i*2] ) == -1 ) {
			fprintf(stderr, "Error creating pipe for thread %d: %s\n", i, strerror(errno));
			return -1;
		}		
	}

	for( i = 0; i < manager->num_threads; i++ ) {		
		if( pthread_create(&manager->threads[i], NULL, &compute_thread_function, &args[i]) ) {
			perror( "Failed creating thread" );
			return -1;
		}		

		if( pthread_create(&manager->callBack_threads[i], NULL, &callback_thread_function, &args[i]) ) {
			perror( "Failed creating thread" );
			return -1;
		}
	}

// Unlock the mutex 
	if( pthread_mutex_unlock(&manager->mutex) ) {
		perror("Unlocking thread mutex");
		return -1;
	}
/*** CRITICAL SECTION END ***/

	return(manager->num_threads);
}

/*****************************************************************************
 * Adds a job to the priority queue
 *****************************************************************************/
int
addjob(	thread_manager_t *manager, int priority,
		thread_manager_function_t job, void *arg,
		thread_manager_function_t callback, void *udata ) {
	
	task_data_t *tag = (task_data_t *)malloc(sizeof(task_data_t));
	if( tag == NULL ) {
		perror("Allocating RAM for SPU process entry");
		return 1;
	}
	
	tag->job		= job;
	tag->arg		= arg;	
	tag->priority	= priority;
	tag->callback	= callback;
	tag->tag		= udata;
	
// Get wall-clock time for job enquement
	clock_gettime(CLOCK_REALTIME, &tag->enqueued);
	enqueue_block(&manager->queue, priority, tag);
	
	return 0;
}

/*****************************************************************************
 * Halts execution and cleans up all state (terminates threads, etc)
 *****************************************************************************/
int
cleanup(thread_manager_t *manager, int priority) {
	int i;
	
	void *value;
	
	// Send a terminate message to the dispatcher thread
	for( i = 0; i < manager->num_threads * 10; i++ ) {
		enqueue_block(&manager->queue, priority, NULL);
	}
			
	// Wait for the SPU_thread to complete execution
	for( i = 0; i < manager->num_threads; i++ ) {
	
	// Join PPU thread presumably the one that got the message
		if( pthread_join(manager->threads[i], &value) ) {
			perror( "Failed to join thread" );
			exit(1);
		}

		if( pthread_join(manager->callBack_threads[i], &value) ) {
			perror( "Failed to join thread" );
			exit(1);
		}
	} 

	if( pthread_mutex_destroy(&manager->mutex) ) {
		perror("Destroying thread mutex");
	}

	// Free the thread array
	free( manager->threads );
	return(0);
}
