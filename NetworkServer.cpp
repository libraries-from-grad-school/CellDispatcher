#include "NetworkServer.h"
#include "NetworkSession.h"

#include <sys/types.h>
#include <sys/socket.h>
#include <errno.h>
#include <unistd.h>
#include <netdb.h>
#include <signal.h>
#include <stdio.h>

int
ignoreSigpipe()
{
	sigset_t newSigSet;
	
	sigemptyset(&newSigSet);
	sigaddset(&newSigSet, SIGPIPE);
	return sigprocmask(SIG_BLOCK, &newSigSet, NULL);
}

NetworkServer::NetworkServer(int port)
{
	struct sockaddr_in server;
	
	if( (ignoreSigpipe() != 0) ||
		((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) ) {
		perror("Creating socket");
		error = true;
		return;
	}
	
/* Enable address reuse */
	int on = 1;
	setsockopt( sock, SOL_SOCKET, SO_REUSEADDR, &on, sizeof(on) );
		
	server.sin_family = AF_INET;
	server.sin_addr.s_addr = INADDR_ANY;
	server.sin_port = htons( (short)port );
	
	if( (bind(sock, (struct sockaddr*)&server, sizeof(server)) < 0 ) ||
		(listen(sock, SOMAXCONN) < 0 ) ) {
		perror("Binding to port");
		error = true;
		return;
	}
	
	error = false;
	return;
}

NetworkSession *
NetworkServer::listen_blocking()
{
	// Listen for an incoming connection indefinitely 
	struct sockaddr_in net_client;
	socklen_t len = sizeof(struct sockaddr_in);
	int retval;
	NetworkSession *theSession = NULL;
	
	fprintf(stderr, "Wating for connection.\n");

	// Listen for a connection (loop if interrupted)
	while( ((retval = accept(sock, (struct sockaddr*)(&net_client), &len )) == -1) &&
			(errno == EINTR) )
	{
		fprintf(stderr, "Interrupted, resuming waiting.\n");
	}

// If the listen was successful create a session object	
	if( retval != -1 ) {
		theSession = new NetworkSession( retval, &net_client );
	} else {
		fprintf(stderr, "Connection attempt failed.\n");
	}
	
	// Return the session object (it's null in case of error)
	return theSession;
}

bool
NetworkServer::isError()
{
	return error;
}

bool
NetworkServer::listen_noblock()
{
	fprintf(stderr, "Non-blocking listening not yet implemented!\n");

// TODO: Spawn a thread to execute the blocking listen method continuously

	return false;
}

void
NetworkServer::listen_stop()
{
	// Kill-off the listening method thread to stop
	return;
}

NetworkServer::~NetworkServer()
{
	// Kill-off the listening method thread and clean-up
}
