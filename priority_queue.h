/*****************************************************************************
 * (C) Oregon State University 2007
 *
 * priority_queue.c - Priority Queue for use with the SPU dispatcher function
 * Author: William Dillon
 *****************************************************************************/

#ifndef PRIORITY_QUEUE_H
#define PRIORITY_QUEUE_H

#include <pthread.h>
#include <semaphore.h>

#ifndef SIZE
#define SIZE 128
#endif

#define INVALID -1

#define TRUE  1
#define FALSE 0

//#define TRACE SIZE*2

#ifdef TRACE
#define ENQUEUE 0
#define DEQUEUE 1

typedef struct trace_struct_t {
	int priority;
	void * tag;
	
	int	op;
	
	int	fill_level;
	int index;
	int swaps;
} trace_t;
#endif

/*****************************************************************************
 * Priority queue entry type structure
 *****************************************************************************/
typedef struct priority_queue_entry {

	// Entry priority (real priorities are non-negative)
	int priority;

	// User storage (may be NULL because the user may enter a NULL value)
	/*@null@*/void *tag;
		
} priority_queue_entry_t;

/*****************************************************************************
 * Priority queue structure
 *****************************************************************************/
typedef struct priority_queue_data {

	// Sorted heap (priority queue)
	priority_queue_entry_t heap[SIZE];	// The heap as an array of structs
	priority_queue_entry_t sentry;		// Help prevent overwriting
	
	// Number of elements in the queue
	unsigned long int num_elements;
	
	// Condition Variables for empty and full queues
	pthread_cond_t cond; //empty, full;
	
	// mutex to ensure data structure sanity in threaded environments
	pthread_mutex_t	mutex;

} priority_queue_data_t;

/*****************************************************************************
 *************************** PUBLIC FUNCTIONS ********************************
 *****************************************************************************/

// Initialize queue data structure
int	   	   init_queue( priority_queue_data_t *queue );

// Enqueue functions, blocking and non-blocking.  non-blocking returns failure if full 
void enqueue_block   ( priority_queue_data_t *queue, int priority, void *tag );
bool enqueue_nonBlock( priority_queue_data_t *queue, int priority, void *tag );

// Test whether the queue is empty, return true if it is.
bool			empty( priority_queue_data_t *queue );

// Get the top element of the queue, blocks on an empty queue
/*@null@*/ // May be NULL because the user may add a NULL entry
void *	   	  get_top( priority_queue_data_t *queue );

#endif
