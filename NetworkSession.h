#ifndef NETWORKSESSION_H_
#define NETWORKSESSION_H_

#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>

#include <limits.h>

class NetworkSession
{
public:
	NetworkSession(char *hostName, unsigned int portNum);
	NetworkSession(int fileDesctiptor, struct sockaddr_in* net_client);
	virtual ~NetworkSession();
	
// Status Queries
	bool  isError();
	bool  isConnected();
	char* getHostName();
	unsigned int getBytesWritten();
	unsigned int getBytesRead();

// I/O methods
	int	getFileDescriptor();
	int writeBytes(void* buffer, int length);
	int readBytes(void* buffer, int length);

// Bit'o storage for user
	int connectionNumber;

	bool error;
private:
	bool connected;
	unsigned int bytesWritten, bytesRead;

	int fileDescriptor;
	char hostName[MAX_CANON];

	struct sockaddr_in server;
	struct hostent *hp;
};

#endif /*NETWORKSESSION_H_*/
