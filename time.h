/*
 *  time.h
 *
 *  Created by William Dillon on 3/19/08.
 *	Copyright 2008 Oregon State University. All rights reserved.
 *
 */

/* COPIED FROM LINUX /usr/include/linux/time.h */

#ifndef _STRUCT_TIMESPEC
#define _STRUCT_TIMESPEC
struct timespec {
	time_t  tv_sec;         /* seconds */
	long    tv_nsec;        /* nanoseconds */
};
#endif

/*
 * The IDs of the various system clocks (for POSIX.1b interval timers):
 */
#define CLOCK_REALTIME                  0
#define CLOCK_MONOTONIC                 1
#define CLOCK_PROCESS_CPUTIME_ID        2
#define CLOCK_THREAD_CPUTIME_ID         3

/* END LINUX COPY */

typedef int clockid_t;

long clock_gettime(clockid_t which_clock, struct timespec *tp);